/** @file Pasteur.h
    @brief Definition of the Pasteur model class

    @attention This file is part of LIBPF
    @attention All rights reserved; do not distribute without permission.
    @author (C) Copyright 2014-2024 Simevo s.r.l.
 */


#ifndef PASTEUR_H
#define PASTEUR_H

/* SYSTEM INCLUDES */

/* PROJECT INCLUDES */
#include <libpf/core/Model.h>
#include <libpf/flowsheet/FlowSheet.h>

/* LOCAL INCLUDES */

/* FORWARD REFERENCES */


/*====================================================================================================================*/

/*==  Pasteur class implementation  ==================================================================================*/

/*====================================================================================================================*/

/** @class Pasteur
 * Generic continuous pasteurization process
 *
 * #include "Pasteur.h"
 *
 */
// SNIPPET 001 start
class Pasteur : public FlowSheet {
private:
  const static std::string type_;
public:
  // LIFECYCLE
  Pasteur(Libpf::Persistency::Defaults defaults, uint32_t id=0, Persistency *persistency=nullptr, Persistent *parent=nullptr, Persistent *root=nullptr);

  // CUSTOM variables
  QUANTITY(Qin, "Thermal power input", 0.0, "W");
  QUANTITY(Win, "Work power input", 0.0, "W");
  QUANTITY(Qout, "Thermal power output", 0.0, "W");
  QUANTITY(Acooler, "Cooler exchange surface", 1.0, "m^2");
  QUANTITY(Aheater, "Heater exchange surface", 1.0, "m^2");
  QUANTITY(Arecuperator, "Recuperator exchange surface", 1.0, "m^2");
  QUANTITY(coolT, "Cooling temperature", 5.0 + 273.15, "K");
  QUANTITY(highT, "Heating temperature", 90.0 + 273.15, "K");
  QUANTITY(holdTime, "Time to keep fluid to designed temperature", 0.50, "s");
  QUANTITY(dTexchangeLL, "Delta T approach for liq-liq exchanger", 20.0, "K");
  QUANTITY(feed, "Feedstock fluid mass flow", 1.0, "kg/s");
  QUANTITY(press, "Fluid outlet relative pressure", 10.0, "bar");
  QUANTITY(hs, "Design speed in the holding pipe", 1.0, "m/s");

  // parameters
  // supported values: HTST25, HTST15, HHST3, HHST1 and user (default)
  STRING(processType, "Process type selection - It selects Temperature and time for the pasteurization", "user");
  // supported values: milkWhole, chocolateIceCream, eggWhole, vanillaFatFreeIceCream and user (default)
  STRING(feedType, "Feed type selection - It selects the fluid composition to be pasteurization", "user");

  // CUSTOM function

  // MANDATORY
  const std::string &type(void) const { return type_; }
  void makeUserEquations(std::list<Assignment *>::iterator &p);
  void setup(void);
  void pre(SolutionMode solutionMode, int level) { }
  void post(SolutionMode solutionMode, int level);

  // NON-MANDATORY
  bool supportsSimultaneous(void);
  int maximumIterations(void);
  int sequential(void);
}; // Pasteur
// SNIPPET end

#endif // PASTEUR_H
