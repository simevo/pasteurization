# README

This the public source code repository for the **pasteurization demo**.

The Pasteurization demo is a process model for continuous (i.e. flash) pasteurization processes implemented in C++ using **LIBPF™**(**LIB**rary for **P**rocess **F**lowsheeting), a **flexible** technology to **model** continuous industrial processes at **steady state**.

The Pasteurization demo (C) Copyright 2014-2024 [simevo s.r.l.](https://simevo.com)

For more details see the [pasteurization demo homepage](https://libpf.com/demos/pasteurization/).

## Building

Not mandatory but you can move into a temporary directory:

  `cd $(mktemp -d)`

Clone this very repo (for this to work you must have already [set up SSH keys to communicate with GitLab](https://docs.gitlab.com/ee/user/ssh.html)):

  `git clone git@gitlab.com:simevo/pasteurization.git`

Move to the cloned repo:

  `cd pasteurization`

This repo uses the [GitLab CI](https://docs.gitlab.com/ee/ci/) to build a Debian package for the current stable release. The Debian package can also be built locally or in a Docker container. Most development tasks are automated with a `Makefile`.

### Development

Tested on Debian 12 (bookworm).

#### Local debug build

1. Install the [LIBPF® SDK for Linux](https://libpf.com/docs/manuals/debian_sdk/)

1. Create the build dir and prepare for debug-enabled compilation:

    ```sh
    meson setup --buildtype debug /tmp/pasteur_debug
    ```

1. Build:

    ```sh
    meson compile -C /tmp/pasteur_debug
    ```

The resulting binary will be in `/tmp/pasteur_debug/Qpasteur`.

#### Running the debug build

1. Install the public [Pasteur demo](https://libpf.com/demos/pasteurization/) for Linux

1. Tweak the installed demo to use the debug binary built above:

    ```sh
    sudo rm /usr/share/libpf-runtime/libpf-kernel-pasteur/kernel
    ln -s /tmp/pasteur_debug/Qpasteur /usr/share/libpf-runtime/libpf-kernel-pasteur/kernel
    ```

1. Launch the LIBPF® User Interface ("UIPF") to interact with the model.

### Docker Debian packaging

Start a disposable container using [our preferred base image](https://gitlab.com/simevo/images/-/blob/main/bookworm-libpf/Dockerfile), which already has all the dependencies required [for LIBPF development](https://libpf.com/docs/manuals/debian_dev/) and Debian packaging:

  `docker run --rm -it -v "$HOME/.ccache:/root/.cache/ccache" -v "$PWD:/libpf" -w /libpf registry.gitlab.com/simevo/images/bookworm-libpf`

The command above shares you hosts' [ccache](https://ccache.dev/) cache with the building containers, if you have ccache installed on the host you may need to adapt to your local config.

From within the disposable container, call the `build` target from the `Makefile`:

  `USERNAME=... PASSWORD=... make build`

**NOTE**: use the username and password you have been given for accessing the LIBPF® SDK packages.

If the build succeeds, at the end the Debian package should be in `libpf-kernel-pasteur_*.deb`.

## Additional Makefile targets:

The following targets should preferably be used in a docker container:

  - `apt`: adds `/etc/apt/sources.list.d/libpf-sdk.list` in order to downloads the mandatory dependence `libpf-dev` from a password protected PPA then installs it

  - `build`: first runs `pin` and `apt` then builds the Debian package with `dpkg-buildpackage -uc -us`

  - `pin`: sets in `etc/apt/preferences.d/libpf-dev` the number version for the `debian/control`, revert of target `unpin`

  - `publish`: publishes the Debian package to a local PPA. N.B.: `aptly` package is required, you need to define/substitute the `CI_JOB_ID` variable and also preconfigure a gpg signing key 
  
  - `rsync`: pubishes the local PPA to the simevo public PPA. N.B.: `rsync` and `openssh-client` packages are required and also the ssh keys must have already been exchanged

  - `unpin:` removes the fire `etc/apt/preferences.d/libpf-dev`, revert of target `pin`

  - `update`: edits the version of `libpd-dev` pinned to its installed version
 
The following targets work best if used locally:

  - `check`: tells which version of `libpd-dev` is pinned, installed and/or possibly candidate for and upgrade (note: requires sudo).

  - `check-changelog`: makes sure the current version is released

  - `release`: uses `gbp dch` and `dch` to update the `debian/changelog` and `src/pasteurversion.cc` files, and commits this last edit with the message "Release <new_version>". N.B.: be sure to pull the remote tag if it has not yet been done

## Release workflow

[GitLab Flow](https://about.gitlab.com/topics/version-control/what-is-gitlab-flow/), working on a separate feature branch:

- commit your changes, build locally, test

- when you're happy, update the changelog with `make release` and `git commit -a --amend`

- push your changes and wait for the CI to build the package, then test the generated `.deb` (you can download it from the artifacts)

- if you're happy, merge to `main` branch using the Gitlab UI; this pipeline will fail (dont't worry)

- locally switch to `main`, pull, then `make release` once more to mark the actual release (you may need to manually amend the changelog)

- push your changes and wait for the CI to build, publish and rsync to https://libpf.com/debs/public/.

Simplified flow, working directly on `main` branch:

- commit your changes, build locally, test

- when you're happy, mark the actual release with `make release` (you may need to manually amend the changelog)

- push your changes and wait for the CI to build, publish and rsync to https://libpf.com/debs/public/.