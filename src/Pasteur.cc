/** @file Pasteur.cc
    @brief declaration/implementation of Pasteur model class

    @attention This file is part of LIBPF
    @attention All rights reserved; do not distribute without permission.
    @author (C) Copyright 2014-2024 Simevo s.r.l.
 */

/* SYSTEM INCLUDES */
//
#define _USE_MATH_DEFINES
#include <cmath> // for M_PI

/* PROJECT INCLUDES */
//
#include <libpf/utility/diagnostic.h>
#include <libpf/streams/Stream.h>
#include <libpf/components/ListComponents.h> // for components and NCOMPONENTS

/* LOCAL INCLUDES */
//
#include "Pasteur.h"

/* LOCAL VARIABLES */
//
static const int verbosityFile = 0;

/* FUNCTIONS */
// 

/*====================================================================================================================*/
/*==  Pasteur class implementation  ==================================================================================*/
/*====================================================================================================================*/

const std::string Pasteur::type_("Pasteur");

Pasteur::Pasteur(Libpf::Persistency::Defaults defaults, uint32_t id, Persistency *persistency, Persistent *parent, Persistent *root) :
  Model(defaults, id, persistency, parent, root),
  VertexBase(defaults, id, persistency, parent, root),
  FlowSheet(defaults, id, persistency, parent, root)
{
  static const int verbosityLocal = 0;
  diagnostic(2, "Entered");
// SNIPPET 001 start
  addVariable(Qin);
  addVariable(Win);
  addVariable(Qout);
  addVariable(Acooler);
  addVariable(Aheater);
  addVariable(Arecuperator);
  addVariable(coolT);
  addVariable(highT);
  addVariable(holdTime);
  addVariable(dTexchangeLL);
  addVariable(feed);
  addVariable(press);
  addVariable(hs);  
  addVariable(processType);
  addVariable(feedType);
// SNIPPET end

// SNIPPET 002 start
  // retrieve string options
  processType = retrieveString(defaults, id, persistency,
                               "processType", // enumerator
                               "user"); // default value
  feedType = retrieveString(defaults, id, persistency,
                            "feedType", // enumerator
                            "user"); // default value
// SNIPPET end
  
  if (!persistency) {
    diagnostic(2, "Define unit operations");
// SNIPPET 003 start
    addUnit("Exchanger", defaults.relay("REC",  "Recuperator"));
    addUnit("Pump", defaults.relay("PUMP", "Liquid circulation pump"));
    addUnit("FlashDrum", defaults.relay("HEATER",  "Pasteurization heater"));
    addUnit("Pipe", defaults.relay("HOLD",  "Holding pipe"));
    addUnit("FlashDrum", defaults.relay("COOLER",  "Cooler"));
// SNIPPET end
    
    diagnostic(2, "Define stream objects and connect");
// SNIPPET 004 start
    addStream("StreamLiquid", defaults.relay("S01", "Fluid inlet"), "source", "out", "REC", "coldin");
    addStream("StreamLiquid", defaults.relay("S02", "Pre-heated fluid"), "REC", "coldout", "PUMP", "in");
    addStream("StreamLiquid", defaults.relay("S03", "Pumped pre-heated fluid"), "PUMP", "out", "HEATER", "in");
    addStream("StreamLiquid", defaults.relay("S04", "Holding hot fluid"), "HEATER", "out", "HOLD", "in");
    addStream("StreamLiquid", defaults.relay("S05", "Pasteurized fluid"), "HOLD", "out", "REC", "hotin");
    addStream("StreamLiquid", defaults.relay("S06", "Warm fluid"), "REC", "hotout", "COOLER", "in");
    addStream("StreamLiquid", defaults.relay("S07", "Cooled fluid outlet"), "COOLER", "out", "sink", "in");
// SNIPPET end
  }
// SNIPPET 005 start
  my_cast<VertexBase *>(&at("HEATER"), CURRENT_FUNCTION)->setIcon("Exchanger", 100.0, 100.0);
  my_cast<VertexBase *>(&at("COOLER"), CURRENT_FUNCTION)->setIcon("Exchanger", 60.0, 60.0);
  my_cast<VertexBase *>(&at("PUMP"), CURRENT_FUNCTION)->setIcon("Pump", 40.0, 40.0); // just to resize the pump icon
  my_cast<VertexBase *>(&at("HOLD"), CURRENT_FUNCTION)->setIcon("Pipe", 100.0, 20.0); // just to stretch the pipe icon
// SNIPPET end
} // Pasteur::Pasteur

void Pasteur::setup(void) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, " entered for " << tag());

    diagnostic(3, "Calling flowsheet::setup to initialize any embedded flowsheet");
    FlowSheet::setup();

    diagnostic(3, "Setting input variables");
    
// SNIPPET 006 start
    highT.unSetInput();
    highT.setOutput();
    holdTime.unSetInput();
    holdTime.setOutput();

    // "Grade A Pasteurized Milk Ordinance" - 2011 Revision
    // https://www.fda.gov/food/hazard-analysis-critical-control-point-haccp/dairy-grade-voluntary-haccp
    // table pag. 8 (pag. 25 of the PDF)

    // High Temperature Short Time
    if (processType.value() == "HTST25") {
      highT = Value(80.0 + 273.15, "K");
      holdTime = Value(25.0, "s");
    } // HTST25
    else if (processType.value() == "HTST15") {
      highT = Value(83.0 + 273.15, "K");
      holdTime = Value(15.0, "s");
    } // HTST15
    // Higher Heat Shorter Time
    else if (processType.value() == "HHST3") {
      highT = Value(90.0 + 273.15, "K");
      holdTime = Value(3.0, "s");
    } // HHST3
    else if (processType.value() == "HHST1") {
      highT = Value(90.0 + 273.15, "K");
      holdTime = Value(1.0, "s");
    } // HHST1
    else if (processType.value() == "user") {
      // default process settings
      highT.set(90.0 + 273.15, "K");
      holdTime.set(1.0, "s");
      highT.setInput();
      holdTime.setInput();
      highT.unSetOutput();
      holdTime.unSetOutput();
    } else {
      throw ErrorRunTime("unsupported processType", CURRENT_FUNCTION);
    }
// SNIPPET end

// SNIPPET 007 start
    // Fluid inlet
    Q("S01.T").set(4.0+273.15,"K"); // typical storage temperature for milk products and ice cream mix
    Q("S01.P").set(1.0, "atm");
    my_cast<Stream *>(&at("S01"), CURRENT_FUNCTION)->setFlash(Libpf::Utility::FlashMode::PT);
    S("S01.flowoption") = "Mx";
    Q("S01:Tphase.mdot").set(feed); // assignement

    my_cast<Stream *>(&at("S01"), CURRENT_FUNCTION)->clearComposition();
    // Feed compositions from:   
    // USDA (United States Department of Agriculture) Agricultural Research Service
    // National Nutrient Database for Standard Reference, Release 26 
    // https://data.nal.usda.gov/dataset/usda-national-nutrient-database-standard-reference-legacy-release
    if (feedType.value() == "milkWhole") {
      // reference n. 01077 "Milk, whole, 3.25% milkfat, with added vitamin D", unnormalized sum = 0.9933
      Q("S01:Tphase.x", "water") = Value(0.8813);
      Q("S01:Tphase.x", "prote") = Value(0.0315);
      Q("S01:Tphase.x", "lipid") = Value(0.0325);
      Q("S01:Tphase.x", "carbo") = Value(0.0480);
      Q("S01:Tphase.x", "fiber") = Value(0.0);
    } // milkWhole
    else if (feedType.value() == "chocolateIceCream") {
      // reference n. 01236 "Ice cream, soft serve, chocolate", unnormalized sum = 0.998
      Q("S01:Tphase.x", "water") = Value(0.598);
      Q("S01:Tphase.x", "prote") = Value(0.041);
      Q("S01:Tphase.x", "lipid") = Value(0.130);
      Q("S01:Tphase.x", "carbo") = Value(0.222);
      Q("S01:Tphase.x", "fiber") = Value(0.007);
    } // chocolateIceCream
    else if (feedType.value() == "eggWhole") {
      // reference n. 01123 "Egg, whole, raw, fresh", unnormalized sum = 1.0542
      Q("S01:Tphase.x", "water") = Value(0.7615);
      Q("S01:Tphase.x", "prote") = Value(0.1256);
      Q("S01:Tphase.x", "lipid") = Value(0.0951);
      Q("S01:Tphase.x", "carbo") = Value(0.072);
      Q("S01:Tphase.x", "fiber") = Value(0.0);
    } // eggWhole
    else if (feedType.value() == "vanillaFatFreeIceCream") {
      // reference n. 19877 "Ice creams, BREYERS, 98% Fat Free Vanilla", unnormalized sum = 1.0483
      Q("S01:Tphase.x", "water") = Value(0.6342);
      Q("S01:Tphase.x", "prote") = Value(0.0330);
      Q("S01:Tphase.x", "lipid") = Value(0.0220);
      Q("S01:Tphase.x", "carbo") = Value(0.3051);
      Q("S01:Tphase.x", "fiber") = Value(0.054);
    } // vanillaFatFreeIceCream
    else if (feedType.value() == "user") {
      // user-inputted values, defaults to pure water
      Q("S01:Tphase.x", "water").set(1.0);
      Q("S01:Tphase.x", "prote").set(0.0);
      Q("S01:Tphase.x", "lipid").set(0.0);
      Q("S01:Tphase.x", "carbo").set(0.0);
      Q("S01:Tphase.x", "fiber").set(0.0);

    } else {
      throw ErrorRunTime("unsupported feedType", CURRENT_FUNCTION);
    }
// SNIPPET end

// SNIPPET 008 start
    // Recuperator
    S("REC.hotoption") = "D";
    Q("REC.deltaPhot").set(100.0, "mbar");
    S("REC.coldoption") = "D";
    Q("REC.deltaPcold").set(100.0, "mbar");
    S("REC.option") = "Tcold";
    Q("REC.coldT").set(Q("S01.T") + dTexchangeLL); // assignement
    S("REC.coldflowoption") = "forward";
    S("REC.hotflowoption") = "backward";
    // U overall water-water (Perry's 8ed tab 11.3
    // 200 - 250 BTU / ( h ft^2 degF )
    // 1136 - 1420 J / ( s m^2 K )  (average 1278)
    Q("REC.U").set(1278.0,"W/(m2*K)");

    S("PUMP.option") = "DH";
    Q("PUMP.deltaP").set(- press - Value(1.0,"bar"));// - Q("REC.deltaPcold") - Q("HEATER.deltaP") - Q("HOLD.deltaP") - Q("REC.deltaPhot") - Q("COOLER.deltaP")); // assignement
    Q("PUMP.duty").set(0.0, "W");
    Q("PUMP.etaE").set(0.9);
    Q("PUMP.etaM").set(0.9);

    // Heater
    S("HEATER.option") = "DT";
    Q("HEATER.deltaP").set(100.0, "mbar");
    Q("HEATER.T").set(highT);  // assignement

    // Holding pipe
    // inputs for Pipe: de, s, L, h, eps
    Q("HOLD.de").set(50.0, "mm"); // initial value, overridden by the assignement
    Q("HOLD.s").set(5.0, "mm");
    Q("HOLD.L").set(10.0, "m"); // initial value, overridden by the assignement
    Q("HOLD.h").set(0.0, "m");
    Q("HOLD.eps").set(0.0457, "mm"); // tubo in acciaio commerciale o ghisa 0.0457 mm

    // Cooler
    S("COOLER.option") = "DT";
    Q("COOLER.deltaP").set(200.0, "mbar");
    Q("COOLER.T").set(coolT); // assignement
// SNIPPET end

    diagnostic(3, "Initializing cut streams");
// SNIPPET 009 start
    Q("S05.T").set(highT);
    Q("S05.P").set(Q("S01.P") + Q("REC.deltaPhot") + Q("COOLER.deltaP"));
    S("S05.flowoption") = "Mx";
    Q("S05:Tphase.mdot").set(feed);
    for (int i=0; i<NCOMPONENTS; ++i)
      Q("S05:Tphase.x", i).set(Q("S01:Tphase.x", i));

    diagnostic(3, "Defining cut streams");
    FlowSheet::addCut("S05");
// SNIPPET end

    diagnostic(3, "Making selected outputs visible in GUI");
// SNIPPET 010 start
    Win.setOutput();
    Qin.setOutput();
    Qout.setOutput();
    Acooler.setOutput();
    Aheater.setOutput();
    Arecuperator.setOutput();

    coolT.setInput();
    feed.setInput();
    press.setInput();
    dTexchangeLL.setInput();
// SNIPPET end

  } // try
  catch(Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // Pasteur::setup

void Pasteur::post(SolutionMode solutionMode, int level) {
  static const int verbosityLocal = 0;
  try {
    diagnostic(2, "Entered for " << tag());

// SNIPPET 011 start
    Qin   = - Q("HEATER.duty");
    Win   = - Q("PUMP.We");
    Qout  =   Q("COOLER.duty");
    // U overall water-water (Perry's 8ed tab 11.3
    // 200 - 250 BTU / ( h ft^2 degF )
    // 1136 - 1420 J / ( s m^2 K )  (average 1278)
    Value Uov(1278.0, "W/(m2*K)");
    // improbable if cooling media available
    Acooler = Q("COOLER.duty") / (Uov * dTexchangeLL);
    Aheater = -Q("HEATER.duty") / (Uov * dTexchangeLL);

    Arecuperator = Q("REC.A");
// SNIPPET end

  } // try
  catch(Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // Pasteur::post

void Pasteur::makeUserEquations(std::list<Assignment *>::iterator &p) {
  static const int verbosityLocal = -1;
  try {
    diagnostic(2, " entered for " << tag());
    int i(0);

// SNIPPET 012 start
    MAKEASSIGNMENT(Q("S01:Tphase.mdot"), feed, "Feedstock fluid mass flow");
    MAKEASSIGNMENT(Q("REC.coldT"), Q("S01.T") + dTexchangeLL , "Recuperator hot side temperature" );
    MAKEASSIGNMENT(Q("PUMP.deltaP"), - press - Q("REC.deltaPcold") - Q("HEATER.deltaP") - Q("HOLD.deltaP") - Q("REC.deltaPhot") - Q("COOLER.deltaP"), "Pump outlet pressure");
    MAKEASSIGNMENT(Q("HEATER.T"), highT,  "Heater outlet temperature - process specific");
    MAKEASSIGNMENT(Q("HOLD.L"), hs * holdTime, "Holding pipe length");
    MAKEASSIGNMENT(Q("HOLD.de"), 2.0 * Q("HOLD.s") + (Q("HOLD.de") - 2.0 * Q("HOLD.s")) * sqrt(holdTime / Q("HOLD.tau")), "Holding pipe diameter");
    MAKEASSIGNMENT(Q("COOLER.T"), coolT, "Cooler outlet temperature");
// SNIPPET end

  } // try
  catch(Error &e) {
    e.append(CURRENT_FUNCTION);
    throw;
  } // catch
} // Pasteur::makeUserEquations


// SNIPPET 013 start
int Pasteur::maximumIterations(void) { return 100; }  // Pasteur::maximumIterations

int Pasteur::sequential(void) { return 5; }  // Pasteur::sequential

bool Pasteur::supportsSimultaneous(void) { return true; }; // Pasteur::supportsSimultaneous
// SNIPPET end
