/** @file PasteurDriver.cc
    @brief implement kernel API for Pasteur

    @attention This file is part of LIBPF
    @attention All rights reserved; do not distribute without permission.
    @author (C) Copyright 2014-2024 Simevo s.r.l.
 */

/* SYSTEM/GENERAL PURPOSE INCLUDES */
//
#define _CRT_SECURE_NO_WARNINGS
#include <string>

/* LIBPF INCLUDES */
//
#include <libpf/core/Libpf.h> // for initializeLibpf / uninitializeLibpf
#include <libpf/utility/version.h>
#include <libpf/components/components.h>
#include <libpf/components/ComponentBiochem.h>
#include <libpf/persistency/NodeFactory.h>
#include <libpf/flowsheet/flowsheets.h> // register all flowsheet types with the model factory
#include <libpf/units/units.h> // register all unit operation types with the model factory
#include <libpf/streams/streams.h> // register all stream types with the model factory
#include <libpf/phases/phases.h> // register all phase types with the model factory
#include <libpf/units/reactions.h> // register all reaction types with the model factory
#include <libpf/components/ListComponents.h>       // for NCOMPONENTS
#include <libpf/user/main.h> // for the main function

/* LOCAL INCLUDES */
//
#include <libpf/user/Kernel.h>

// SNIPPET 001 start
// include specific headers
#include <Pasteur.h>
// SNIPPET 001 end

/* FORWARD REFERENCES */
//

/* MACROS */
// avoid macros!!
//

/* GLOBAL CONSTANTS */
//

/* GLOBAL VARIABLES */
//

/* FUNCTIONS */
//
void setComponents(void); // this will be provided by the driver

// SNIPPET 002 start
// register the service
Libpf::User::KernelImplementation impl_("Pasteurize", // name
  "Process modeling of continuous pasteurization processes", // description
  "(C) Copyright 2014-2024 Simevo s.r.l.", // license
  "0.1.17 [Wed Feb 5 14:14:13 2025 +0100]", // version
  "Pasteur", // defaultType
  "62e3a255-c1a0-4dea-a650-05b9a4a33aef" // uuid
);
// SNIPPET 002 end

void Libpf::User::initializeKernel(void) {
  // initialize library
  if (initializeLibpf()) {

    NodeFactory nodeFactory;
// SNIPPET 003 start
    // register the provided types
    nodeFactory.registerType<Pasteur>("Pasteur", // type name
                                      "Generic continuous pasteurization process", // type description
                                      "flowsheet", // type category
                                      true, // can be instantiated as a to-level model (Case)
                                      { }, // integer options
                                      { Libpf::Persistency::StringOption("processType", "adjusts temperature and holding time", "HTST15", "processType"),
                                      Libpf::Persistency::StringOption("feedType", "sets a predefined composition for the fluid to be processed", "chocolateIceCream", "feedType") }, // string options
                                      { }, // quantity options
                                      "Pasteur", 80.0, 80.0); // icon and icon size
    nodeFactory.registerAlias("Pasteur", // base type
                              "PasteurHTST15_milkWhole", // alias
                              "High Temperature Short Time Pasteurization 15 s of whole milk", // alias description
                              Libpf::Persistency::Defaults()("processType", "HTST15")("feedType","milkWhole"), "PasteurHTST15_milkWhole", 80.0, 80.0); // icon and icon size
// SNIPPET 003 end

    components.clear();
// SNIPPET 004 start
    // populate the list of components
    components.addcomp(new purecomps::water("water"));
    components.addcomp(new purecomps::Protein("prote"));
    components.addcomp(new purecomps::Lipid("lipid"));
    components.addcomp(new purecomps::Carbohydrate("carbo"));
    components.addcomp(new purecomps::Fiber("fiber"));
// SNIPPET 004 end

    // populate locales
    Libpf::User::addLocale("en");
    
// SNIPPET 005 start
    // populate enumerators
    // supported values: HTST25, HTST15, HHST3, HHST1 and user (default)
    Libpf::Core::Enumerator processType("processType", "Process type selection - It selects Temperature and time for the pasteurization");
    processType.addOption(Libpf::Utility::Option("HTST25", "High Temperature Short Time 25 s @80 C"));
    processType.addOption(Libpf::Utility::Option("HTST15", "High Temperature Short Time 15 s @83 C"));
    processType.addOption(Libpf::Utility::Option("HHST3", "Higher Heat Shorter Time 3 s @90 C"));
    processType.addOption(Libpf::Utility::Option("HHST1", "Higher Heat Shorter Time 1 s @90 C"));
    processType.addOption(Libpf::Utility::Option("user", "(default) user specified"));
    Libpf::User::addEnumerator(processType);
    // supported values: milkWhole, chocolateIceCream, eggWhole, vanillaFatFreeIceCream and user (default)
    Libpf::Core::Enumerator feedType("feedType", "Feed type selection - It selects the fluid composition to be pasteurization");
    feedType.addOption(Libpf::Utility::Option("milkWhole", "Milk, whole, 3.25% milkfat, with added vitamin D"));
    feedType.addOption(Libpf::Utility::Option("chocolateIceCream", "Ice cream, soft serve, chocolate"));
    feedType.addOption(Libpf::Utility::Option("eggWhole", "Egg, whole, raw, fresh"));
    feedType.addOption(Libpf::Utility::Option("vanillaFatFreeIceCream", "Ice creams, BREYERS, 98% Fat Free Vanilla"));
    feedType.addOption(Libpf::Utility::Option("user", "(default) user specified defaults to pure water"));
    Libpf::User::addEnumerator(feedType);
    Libpf::Utility::SmartEnumerator<Libpf::Utility::ScalingMode> sm;
    Libpf::User::addEnumerator(sm);
// SNIPPET 005 end
  } // the library has actually been initialized
} // initializeKernel

void Libpf::User::uninitializeKernel(void) {
  if (uninitializeLibpf()) {
    components.clear();
  } // the library has actually been uninitialized
} // uninitializeKernel

int main(int argc, char *argv []) {
  return Libpf::User::main(argc, argv);
}
