DEBEMAIL = "developers@libpf.com"
DEBFULLNAME = "simevo s.r.l"
GIT_BRANCH_CURRENT = $(shell git rev-parse --abbrev-ref HEAD)
GIT_BRANCH_DEFAULT = "main"
GIT_DATE = $(shell git log -n1 | grep '^Date:' | sed 's/Date: *\(.*\)/\1/g')
GIT_MAJOR = $(shell git describe --long | cut -d- -f1 | cut -d. -f1)
GIT_MINOR = $(shell git describe --long | cut -d- -f1 | cut -d. -f2)
GIT_PATCH = $(shell git describe --long | cut -d- -f2)
GIT_VERSION_NEXT = $(GIT_MAJOR)"."$(GIT_MINOR)"."$(shell echo $$(($(GIT_PATCH)+1)))
PACKAGE_NAME_DEV = "libpf-dev"
PACKAGE_NAME_RUNTIME = "libpf-runtime"
SNAPSHOT_LIST=$(shell aptly snapshot list -raw)
VERSION_CANDIDATE = $(shell LANG=en apt-cache policy $(PACKAGE_NAME_DEV) | grep '^  Candidate:' | grep -oe '\([0-9.]*\)')
VERSION_INSTALLED = $(shell LANG=en apt-cache policy $(PACKAGE_NAME_DEV) | grep '^  Installed:' | grep -oe '\([0-9.]*\)')
VERSION_PINNED_DEV = $(shell cat debian/control | grep $(PACKAGE_NAME_DEV) |  grep -oe '\([0-9.]*\)')
VERSION_PINNED_RUNTIME = $(shell cat debian/control | grep $(PACKAGE_NAME_RUNTIME) |  grep -oe '\([0-9.]*\)')

pin:
	@echo "Package: libpf-dev" | sudo sh -c "cat > /etc/apt/preferences.d/libpf-dev"
	@echo "Pin: version $(VERSION_PINNED_DEV)" | sudo sh -c "cat >> /etc/apt/preferences.d/libpf-dev"
	@echo "Pin-Priority: 510" | sudo sh -c "cat >> /etc/apt/preferences.d/libpf-dev"

unpin:
	@sudo rm /etc/apt/preferences.d/libpf-dev

apt:
	@if [ -z $(USERNAME) ]; then \
		echo "ERROR: USERNAME is not defined."; \
		exit 1; \
	fi
	@if [ -z $(PASSWORD) ]; then \
		echo "ERROR: PASSWORD is not defined."; \
		exit 2; \
	fi
	@wget -q -O - https://$(USERNAME):$(PASSWORD)@libpf.com/debs/sdk/libpf.gpg.key | sudo apt-key add -
	@echo "deb https://$(USERNAME):$(PASSWORD)@libpf.com/debs/sdk/ bookworm main" | sudo sh -c "cat > /etc/apt/sources.list.d/libpf-sdk.list"
	@sudo apt update
	@sudo apt install libpf-dev

build: pin apt
	@dpkg-buildpackage -uc -us
	mv ../libpf-kernel* .

clean:
	@for i in $(SNAPSHOT_LIST);	do \
		echo $$i; \
		if [ $$i != "snapshot-public-$(CI_PIPELINE_ID)" ]; then \
			aptly publish drop bookworm $$i; \
			aptly snapshot -force drop $$i; \
		fi \
	done
	@aptly db cleanup -verbose 

publish: clean
	# only once:
	# aptly repo create -distribution=bookworm -component=main libpf-kernels
	aptly repo add libpf-kernels libpf-kernel-*_amd64.deb
	aptly repo show -with-packages libpf-kernels

	# create snapshot from the local repo
	aptly snapshot create snapshot-libpf-kernels-$(CI_PIPELINE_ID) from repo libpf-kernels

	# mirror remote staging repo where simevo uploads the libpf-runtime and libpf-ui packages
	wget -q -O - https://libpf.com/debs/public_internal/libpf.gpg.key | gpg --no-default-keyring --keyring trustedkeys.gpg --import
	# only once:
	# aptly mirror create staging https://libpf.com/debs/public_internal/ bookworm
	aptly mirror update staging
	aptly mirror show -with-packages staging

	# create snapshot from my mirror of remote repo
	aptly snapshot create snapshot-staging-$(CI_PIPELINE_ID) from mirror staging

	# merge staging with libpf-kernels
	aptly snapshot merge snapshot-public-$(CI_PIPELINE_ID) snapshot-staging-$(CI_PIPELINE_ID) snapshot-libpf-kernels-$(CI_PIPELINE_ID)

	aptly publish snapshot snapshot-public-$(CI_PIPELINE_ID) snapshot-public-$(CI_PIPELINE_ID)

rsync:
	rsync -avz "$$HOME/.aptly/public/snapshot-public-$(CI_PIPELINE_ID)/" /var/www/html/debs/public

release:
	@DEBEMAIL=$(DEBEMAIL) DEBFULLNAME=$(DEBFULLNAME) gbp dch --new-version=$(GIT_VERSION_NEXT) --ignore-branch --no-multimaint
	@sed -i "s/^  \"[0-9]*\.[0-9]*\.[0-9]* \[.*\]\", \/\/ version$$/  \"$(GIT_VERSION_NEXT) [$(GIT_DATE)]\", \/\/ version/g" src/PasteurDriver.cc
	@sed -i "s/This is Pasteurization version .*\./This is Pasteurization version $(GIT_VERSION_NEXT)./g" LICENSE
	@if [ "$(GIT_BRANCH_CURRENT)" = "$(GIT_BRANCH_DEFAULT)" ]; then \
		DEBEMAIL=$(DEBEMAIL) DEBFULLNAME=$(DEBFULLNAME) dch -v $(GIT_VERSION_NEXT) --no-multimaint "Release $(GIT_VERSION_NEXT)"; \
		DEBEMAIL=$(DEBEMAIL) DEBFULLNAME=$(DEBFULLNAME) dch --distribution bookworm -r ""; \
		git commit -a -m "Release $(GIT_VERSION_NEXT)"; \
	fi

check:
	@if [ -z $(VERSION_INSTALLED) ]; then \
		echo "ERROR: $(PACKAGE_NAME_DEV) is not installed."; \
		exit 1; \
	fi
	@if [ "$(VERSION_INSTALLED)" != "$(VERSION_PINNED_DEV)" ]; then \
		echo "ERROR: The installed version ($(VERSION_INSTALLED)) of $(PACKAGE_NAME_DEV) is different from the pinned version ($(VERSION_PINNED_DEV)); you may 'make update'."; \
		exit 2; \
	fi
	@if [ "$(VERSION_CANDIDATE)" != "$(VERSION_PINNED_DEV)" ]; then \
		echo "ERROR: A newer version ($(VERSION_CANDIDATE)) of $(PACKAGE_NAME_DEV) is available; you may 'make apt'."; \
		exit 3; \
	fi
	@echo "$(PACKAGE_NAME_DEV) is up to date."; \

update:
	@if [ "$(VERSION_PINNED_DEV)" != "$(VERSION_PINNED_RUNTIME)" ]; then \
		echo "ERROR: The pinned version ($(VERSION_PINNED_DEV)) of $(PACKAGE_NAME_DEV) is different from the pinned version ($(VERSION_PINNED_RUNTIME)) of $(PACKAGE_NAME_RUNTIME)."; \
		exit 1; \
	fi
	@if [ "$(VERSION_INSTALLED)" != "$(VERSION_PINNED_DEV)" ]; then \
		sed -i "s/$(PACKAGE_NAME_DEV) (=$(VERSION_PINNED_DEV))/$(PACKAGE_NAME_DEV) (=$(VERSION_INSTALLED))/g" debian/control; \
		sed -i "s/$(PACKAGE_NAME_RUNTIME) (=$(VERSION_PINNED_DEV))/$(PACKAGE_NAME_RUNTIME) (=$(VERSION_INSTALLED))/g" debian/control; \
	fi

check-changelog:
	head -n 1 debian/changelog | grep -v UNRELEASED

